package com.sam.hystrix.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by sam on 2018/3/14.
 */
@Service
public class HystrixService {
    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloServiceFallback")
    public String hello() {
        return restTemplate.getForEntity("http://SMILE-SAM-SERVICE/hello", String.class).getBody();
    }

    public String helloServiceFallback() {
        return "error";
    }
}

package com.sam.hystrix.controller;

import com.sam.hystrix.service.HystrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sam on 2018/3/14.
 */
@RestController
public class HystrixController {
    @Autowired
    private HystrixService hystrixService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String add() {
        return hystrixService.hello();
    }
}

package com.sam.controller;

import com.sam.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sam on 2018/3/14.
 */
@RestController
public class HelloController {
    @Autowired
    HelloService helloService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public Object hello() {
        return helloService.hello();
    }

    @RequestMapping(value = "/hi", method = RequestMethod.GET)
    public Object hi(@RequestParam String id) {
        return helloService.hi(id);
    }
}

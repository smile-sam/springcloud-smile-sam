package com.sam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class SmileSamEurekaFeiginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmileSamEurekaFeiginApplication.class, args);
	}
}

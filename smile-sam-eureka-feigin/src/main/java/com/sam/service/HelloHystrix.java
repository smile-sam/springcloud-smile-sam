package com.sam.service;

import org.springframework.stereotype.Component;

/**
 * Created by sam on 2018/3/14.
 */
@Component
public class HelloHystrix implements HelloService {
    @Override
    public String hello() {
        return "服务关闭";
    }

    @Override
    public String hi(String id) {
        return "服务关闭";
    }
}

package com.sam.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by sam on 2018/3/14.
 */
@FeignClient(value = "SMILE-SAM-SERVICE", fallback = HelloHystrix.class)
//第一种 @FeignClient("SMILE-SAM-SERVICE")
public interface HelloService {

    @RequestMapping(method = RequestMethod.GET, value = "/hello")
    String hello();

    @RequestMapping(method = RequestMethod.GET, value = "/hi")
    String hi(@RequestParam(value = "id") String id); // value = id 对应调用服务接口的参数
}

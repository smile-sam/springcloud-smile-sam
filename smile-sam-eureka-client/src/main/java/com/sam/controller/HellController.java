package com.sam.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sam on 2018/3/14.
 */
@RestController
public class HellController {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    @RequestMapping("/hello")
    public String hello() {
        return "hello , project name is " + applicationName + " and port is " + port;
    }
    @RequestMapping("/hi")
    public String hi(@RequestParam String id) {
        return "hi, " + id + ", " + applicationName + ":" + port;
    }
}
